#required packages : texlive-binaries texlive-latex-extra biber
check: pdflatex-exists biber-exists pdflatex-extra-installed
pdflatex-exists: ; @which pdflatex > /dev/null
biber-exists: ; @which biber > /dev/null
pdflatex-extra-installed: ; @dpkg -l texlive-latex-extra > /dev/null

all: check main.tex
	pdflatex -interaction batchmode main.tex
	biber main
	pdflatex -interaction batchmode main.tex
clean:
	rm -fv *.pdf *log *.aux *.toc *.blg *.fdb_latexmk *.bbl *.synctex.gz *.bcf *.run.xml *.out .DS_Store
